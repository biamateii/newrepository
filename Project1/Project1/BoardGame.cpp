#include "BoardGame.h"

const size_t maxDimensionOfBoardLines = 4;
const size_t maxDimensionOfBoardColumns = 3;

BoardGame::BoardGame():m_noLines(maxDimensionOfBoardLines), m_noColumns(maxDimensionOfBoardColumns)
{
}


BoardGame::~BoardGame()
{
}
