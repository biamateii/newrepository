#include "Game.h"

Game::Game()
{
}

Game::~Game()
{
}



bool Game::isSet(const Card &firstCard, const Card &secondCard, const Card &thirdCard) const
{
	if (firstCard.getNumber() != secondCard.getNumber() ||
		firstCard.getNumber() != thirdCard.getNumber())
		return false;
	if (firstCard.getSymbol() != secondCard.getSymbol() ||
		firstCard.getSymbol() != thirdCard.getSymbol())
		return false;
	if (firstCard.getColor() != secondCard.getColor() ||
		firstCard.getColor() != thirdCard.getColor())
		return false;
	if (firstCard.getShading() != secondCard.getShading() ||
		firstCard.getShading() != thirdCard.getShading())
		return false;
}

bool Game::findSet() const
{
	for (int index = 0; index < m_noLines*m_noColumns; index++)
	{
		for (auto index1 = 0; index1 < m_noLines; index1++)
		{
			if (index != index1)
			{
				for (auto index2 = 0; index2 < m_noColumns; index2++)
				{
					if (index != index2)
					{
						if (isSet(m_boardGame[index][index1], m_boardGame[index][index2], m_boardGame[index1][index2]) == true)
							return true;
					}

				}
			}
		}
	}

	return false;
}