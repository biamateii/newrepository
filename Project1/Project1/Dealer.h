#pragma once
#include "BoardGame.h"
#include "Deck.h"

class Dealer:
	public BoardGame, public Deck
{
	
public:
	Dealer();
	~Dealer();

	void dealtCardsOnTheBoard();
};

