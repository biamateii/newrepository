#pragma once
#include <iostream>
class Card
{
public:
	enum class Number { DEFAULT, ONE, TWO, THREE };
	enum class Symbol { DEFAULT, DIAMOND, SQUIGGLE, OVAL };
	enum class Shading { DEFAULT, SOLID, STRIPED, OPEN };
	enum class Color { DEFAULT, RED, GREEN, BLUE };
private:
	Number m_number;
	Symbol m_symbol;
	Shading m_shading;
	Color m_color;
public:
	Card();
	Card(const Number &number,
		const Symbol &symbol,
		const Shading &shading,
		const Color &color);
	Card(const Card &otherCard);

	Card& operator=(const Card &otherCard);
	friend std::ostream& operator<<(std::ostream &os, const Card &card);

	Card getCard() const;

	Number getNumber() const;
	Symbol getSymbol() const;
	Shading getShading() const;
	Color getColor() const;

	~Card();
};

