#include "Card.h"

Card::Card() :
	m_number(Number::DEFAULT),
	m_symbol(Symbol::DEFAULT),
	m_shading(Shading::DEFAULT),
	m_color(Color::DEFAULT)
{
}

Card::Card(const Number &number, const Symbol &symbol, const Shading &shading, const Color &color):
	m_number(number),
	m_symbol(symbol),
	m_shading(shading),
	m_color(color)
{
}

Card::Card(const Card &other)
{
	*this = other;
}

Card::~Card()
{
}

Card& Card::operator=(const Card &otherCard)
{
	m_number = otherCard.m_number;
	m_symbol = otherCard.m_symbol;
	m_shading = otherCard.m_shading;
	m_color = otherCard.m_color;

	return *this;
}

std::ostream& operator<<(std::ostream &os, const Card &card)
{
	switch (card.m_number)
	{
	case Card::Number::ONE:
		os << "1";
		break;
	case Card::Number::TWO:
		os << "2";
		break;
	case Card::Number::THREE:
		os << "3";
		break;
	default:
		break;
	}

	switch (card.m_symbol)
	{
	case Card::Symbol::DIAMOND:
		os << "Di";
		break;
	case Card::Symbol::SQUIGGLE:
		os << "Sq";
		break;
	case Card::Symbol::OVAL:
		os << "Ov";
		break;
	default:
		break;
	}

	switch (card.m_shading)
	{
	case Card::Shading::SOLID:
		os << "So";
		break;
	case Card::Shading::STRIPED:
		os << "St";
		break;
	case Card::Shading::OPEN:
		os << "Op";
		break;
	default:
		break;
	}

	switch (card.m_color)
	{
	case Card::Color::RED:
		os << "Re";
		break;
	case Card::Color::GREEN:
		os << "Gr";
		break;
	case Card::Color::BLUE:
		os << "Bl";
		break;
	default:
		break;
	}

	return os;
}

Card Card::getCard() const
{
	return *this;
}

Card::Number Card::getNumber() const
{
	return m_number;
}

Card::Symbol Card::getSymbol() const
{
	return m_symbol;
}

Card::Shading Card::getShading() const
{
	return m_shading;
}

Card::Color Card::getColor() const
{
	return m_color;
}