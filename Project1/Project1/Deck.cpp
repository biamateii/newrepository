#include "Deck.h"
#include <random>

const size_t maxNumberOfCards = 81;

Deck::Deck() :m_cardIndex(0)
{
	for (size_t index = static_cast<int>(Card::Number::ONE);
		index <= static_cast<int>(Card::Number::THREE);
		index++)
	{
		for (size_t index2 = static_cast<int>(Card::Symbol::DIAMOND);
			index2 <= static_cast<int>(Card::Symbol::OVAL);
			index2++)
		{
			for (size_t index3 = static_cast<int>(Card::Shading::SOLID);
				index3 <= static_cast<int>(Card::Shading::OPEN);
				index3++)
			{
				for (size_t index4 = static_cast<int>(Card::Color::RED);
					index4 <= static_cast<int>(Card::Color::BLUE);
					index4++)
				{
					Card newCard(static_cast<Card::Number>(index),
						static_cast<Card::Symbol>(index2),
						static_cast<Card::Shading>(index3),
						static_cast<Card::Color>(index4));
					m_deck.push_back(newCard);
				}
			}
		}
	}
}

Deck::~Deck()
{
}

void Deck::swap(Card &firstCard, Card &secondCard)
{
	Card tempCard = firstCard;
	firstCard = secondCard;
	secondCard = tempCard;
}

void Deck::shuffleDeck()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, maxNumberOfCards);

	for (auto&& elem : m_deck)
	{
		unsigned int randomCardIndex = dis(gen);
		swap(elem, m_deck.at(randomCardIndex));
	}
}

Card Deck::dealtCard()
{
	if (m_cardIndex == maxNumberOfCards)
	{
		std::cout << "No more cards!\n";
		return Card();
	}
	else
	{
		return m_deck.at(m_cardIndex++);
	}
}