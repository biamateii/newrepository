#pragma once
#include "Card.h"
#include "Deck.h"
#include "BoardGame.h"
#include <vector>

class Game
{
private:
	BoardGame m_boardGame;
	Deck m_deck;
public:
	Game();
	~Game();

	bool isSet(const Card &firstCard, const Card &secondCard, const Card &thirdCard) const;
	bool findSet() const;


};

