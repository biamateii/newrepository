#pragma once
#include "Card.h"
#include <vector>

class Deck
{
protected:
	std::vector<Card> m_deck;
	unsigned int m_cardIndex;
public:
	Deck();
	~Deck();

	void swap(Card &firstCard, Card &secondCard);
	void shuffleDeck();

	Card dealtCard();

};

